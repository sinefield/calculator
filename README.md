# Calculator
This is a simple command line program meant to have at least the functionalites of a common calculator (Add,Subtract and common operators, as well as Memory storage and manipulation)
# Layout
The calculator can be used as a separate program that has it's own terminal GUI, and also as a terminal command to be used in conjunction with other commands via piping or redirection
